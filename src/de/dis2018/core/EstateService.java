package de.dis2018.core;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import de.dis2018.data.House;
import de.dis2018.data.Estate;
import de.dis2018.data.PurchaseContract;
import de.dis2018.data.EstateAgent;
import de.dis2018.data.TenancyContract;
import de.dis2018.data.Person;
import de.dis2018.data.Apartment;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *  Class for managing all database entities.
 *
 * TODO: All data is currently stored in memory.
 * The aim of the exercise is to gradually outsource data management to the
 * database. When the work is done, all sets in this class become obsolete!
 */
public class EstateService {
	//TODO All these sets should be commented out after successful implementation.
//	private Set<EstateAgent> estateAgents = new HashSet<EstateAgent>();
//	private Set<Person> persons = new HashSet<Person>();
//	private Set<House> houses = new HashSet<House>();
//	private Set<Apartment> apartments = new HashSet<Apartment>();
//	private Set<TenancyContract> tenancyContracts = new HashSet<TenancyContract>();
//	private Set<PurchaseContract> purchaseContracts = new HashSet<PurchaseContract>();

	//Hibernate Session
	private SessionFactory sessionFactory;

	public EstateService() {
		sessionFactory = new Configuration().configure().buildSessionFactory();
	}

	// This is to ensure to session is getting open and closed
	private void openExecuteCloseSession(SessionExecution se) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		se.exec(session);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Find an estate agent with the given id
	 * @param id The ID of the agent
	 * @return Agent with ID or null
	 */
	public EstateAgent getEstateAgentByID(int id){
		AtomicReference<EstateAgent> h = new AtomicReference<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<EstateAgent> estateAgent = cq.from(EstateAgent.class);
			cq.select(estateAgent).where(cb.equal(estateAgent.get("id"), id));
			Query q = sess.createQuery(cq);
			h.set((EstateAgent) q.getSingleResult());
		});

		return h.get();
	}

	/**
	 * Find estate agent with the given login.
	 * @param login The login of the estate agent
	 * @return Estate agent with the given ID or null
	 */
	public EstateAgent getEstateAgentByLogin(String login) {
		AtomicReference<EstateAgent> h = new AtomicReference<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<EstateAgent> estateAgent = cq.from(EstateAgent.class);
			ParameterExpression<EstateAgent> p = cb.parameter(EstateAgent.class);
			cq.select(estateAgent).where(cb.equal(estateAgent.get("login"), login));
			Query q = sess.createQuery(cq);
			h.set((EstateAgent) q.getSingleResult());
		});

		return h.get();
	}

	/**
	 * Returns all estateAgents
	 */
	public List<EstateAgent> getAllEstateAgents() {
		ArrayList<EstateAgent> result = new ArrayList<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<EstateAgent> estateAgent = cq.from(EstateAgent.class);
			cq.select(estateAgent);
			Query q = sess.createQuery(cq);
			for (Object o: ((org.hibernate.query.Query) q).list()) {
				result.add((EstateAgent) o);
			}

		});
		return result;
	}

	/**
	 * Find an person with the given id
	 * @param id The ID of the person
	 * @return Person with ID or null
	 */
	public Person getPersonById(int id) {
		AtomicReference<Person> h = new AtomicReference<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<Person> person = cq.from(Person.class);
			cq.select(person).where(cb.equal(person.get("id"), id));
			Query q = sess.createQuery(cq);
			h.set((Person) q.getSingleResult());
		});

		return h.get();
	}

	/**
	 * Adds an estate agent
	 * @param ea The estate agent
	 */
	public void addEstateAgent(EstateAgent ea) {
		openExecuteCloseSession((session -> {
			session.save(ea);
		}));
	}
	public void updateEstateAgent(EstateAgent ea) {
		openExecuteCloseSession((session -> {
			session.update(ea);
		}));
	}

	/**
	 * Deletes an estate agent
	 * @param ea The estate agent
	 */
	public void deleteEstateAgent(EstateAgent ea) {
		openExecuteCloseSession((session -> {
			session.delete(ea);
		}));
	}

	/**
	 * Adds a person
	 * @param p The person
	 */
	public void savePerson(Person p) {
		openExecuteCloseSession((sess) -> {
			sess.save(p);
		});
	}


	public void updatePerson(Person p) {
		openExecuteCloseSession((sess) -> {
			sess.update(p);
		});
	}

	/**
	 * Returns all persons
	 */
	public List<Person> getAllPersons() {
		ArrayList<Person> result = new ArrayList<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<Person> estateAgent = cq.from(Person.class);
			cq.select(estateAgent);
			Query q = sess.createQuery(cq);
			for (Object o: ((org.hibernate.query.Query) q).list()) {
				result.add((Person) o);
			}

		});
		return result;
	}

	/**
	 * Deletes a person
	 * @param p The person
	 */
	public void deletePerson(Person p) {
		openExecuteCloseSession((sess) -> {
			sess.delete(p);
		});
	}

	/**
	 * Adds a house
	 * @param h The house
	 */
//	public void addHouse(House h) {
//		houses.add(h);
//	}

	/**
	 * Returns all houses of an estate agent
	 * @param ea the estate agent
	 * @return All houses managed by the estate agent
	 */
	public List<House> getAllHousesForEstateAgent(EstateAgent ea) {
		ArrayList<House> result = new ArrayList<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<House> houses = cq.from(House.class);
			cq.select(houses).where(cb.equal(houses.get("manager"), ea));
			Query q = sess.createQuery(cq);
			for (Object o: ((org.hibernate.query.Query) q).list()) {
				result.add((House) o);
			}

		});
		return result;
	}

	/**
	 * Find a house with a given ID
	 * @param  id the house id
	 * @return The house or null if not found
	 */
	public House getHouseById(int id) {
		AtomicReference<House> h = new AtomicReference<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<House> houses = cq.from(House.class);
			ParameterExpression<House> p = cb.parameter(House.class);
			cq.select(houses).where(cb.equal(houses.get("id"), id));
			Query q = sess.createQuery(cq);
			h.set((House) q.getSingleResult());
		});

		return h.get();
	}

	public void saveHouse(House h) {
		openExecuteCloseSession((sess) -> {
			sess.save(h);
		});
	}

	public void updateHouse(House h) {
		openExecuteCloseSession((sess) -> {
			sess.update(h);

		});
	}

	public void deleteHouse(House h) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(h);
		session.getTransaction().commit();
		session.close();
	}


	public Apartment getApartmentByID(int id) {
		AtomicReference<Apartment> a = new AtomicReference<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<Apartment> apartments = cq.from(Apartment.class);
			ParameterExpression<Apartment> p = cb.parameter(Apartment.class);
			cq.select(apartments).where(cb.equal(apartments.get("id"), id));
			Query q = sess.createQuery(cq);
			a.set((Apartment) q.getSingleResult());
		});

		return a.get();
	}

	public List<Apartment> getAllApartmentsForEstateAgent(EstateAgent ea) {
		ArrayList<Apartment> result = new ArrayList<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<Apartment> apartments = cq.from(Apartment.class);
			cq.select(apartments).where(cb.equal(apartments.get("manager"), ea));
			Query q = sess.createQuery(cq);
			for (Object o: ((org.hibernate.query.Query) q).list()) {
				result.add((Apartment) o);
			}

		});
		return result;
	}

	public void saveApartment(Apartment a) {
		openExecuteCloseSession((sess) -> {
			sess.save(a);
		});
	}

	public void updateApartment(Apartment a) {
		openExecuteCloseSession((sess) -> {
			sess.update(a);

		});
	}

	public void deleteApartment(Apartment w) {
		openExecuteCloseSession((sess) -> {
			sess.delete(w);

		});
	}


	/**
	 * Adds a tenancy contract
	 * @param t The tenancy contract
	 */
	public void addTenancyContract(TenancyContract t) {
		openExecuteCloseSession((session -> {
			session.save(t);
		}));
	}

	/**
	 * Adds a purchase contract
	 * @param p The purchase contract
	 */
	public void addPurchaseContract(PurchaseContract p) {
		openExecuteCloseSession((sess) -> {
			sess.save(p);

		});
	}

	/**
	 * Finds a tenancy contract with a given ID
	 * @param id Die ID
	 * @return The tenancy contract or zero if not found
	 */
//	public TenancyContract getTenancyContractByID(int id) {
//		Iterator<TenancyContract> it = tenancyContracts.iterator();
//
//		while(it.hasNext()) {
//			TenancyContract m = it.next();
//
//			if(m.getId() == id)
//				return m;
//		}
//
//		return null;
//	}

	/**
	 * Finds a purchase contract with a given ID
	 * @param id The id of the purchase contract
	 * @return The purchase contract or null if not found
	 */
//	public PurchaseContract getPurchaseContractById(int id) {
//		Iterator<PurchaseContract> it = purchaseContracts.iterator();
//
//		while(it.hasNext()) {
//			PurchaseContract k = it.next();
//
//			if(k.getId() == id)
//				return k;
//		}
//
//		return null;
//	}

	/**
	 * Returns all tenancy contracts for apartments of the given estate agent
//	 * @param m The estate agent
	 * @return All contracts belonging to apartments managed by the estate agent
	 */
	public List<TenancyContract> getAllTenancyContractsForEstateAgent(EstateAgent ea) {
		ArrayList<TenancyContract> result = new ArrayList<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<TenancyContract> tenancyContract = cq.from(TenancyContract.class);
			cq.select(tenancyContract).where(cb.equal(tenancyContract.get("apartment").get("manager"), ea));
			Query q = sess.createQuery(cq);
			for (Object o: ((org.hibernate.query.Query) q).list()) {
				result.add((TenancyContract) o);
			}

		});
		return result;
	}

	/**
	 * Returns all purchase contracts for houses of the given estate agent
//	 * @param m The estate agent
	 * @return All purchase contracts belonging to houses managed by the given estate agent
	 */
	public List<PurchaseContract> getAllPurchaseContractsForEstateAgent(EstateAgent ea) {
		ArrayList<PurchaseContract> result = new ArrayList<>();
		openExecuteCloseSession((sess) -> {
			CriteriaBuilder cb = sess.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<PurchaseContract> purchaseContract = cq.from(PurchaseContract.class);
			cq.select(purchaseContract).where(cb.equal(purchaseContract.get("house").get("manager"), ea));
			Query q = sess.createQuery(cq);
			for (Object o: ((org.hibernate.query.Query) q).list()) {
				result.add((PurchaseContract) o);
			}

		});
		return result;
	}

	/**
	 * Finds all tenancy contracts relating to the apartments of a given estate agent
	 * @param ea The estate agent
	 * @return set of tenancy contracts
	 */
//	public Set<TenancyContract> getTenancyContractByEstateAgent(EstateAgent ea) {
//		Set<TenancyContract> ret = new HashSet<TenancyContract>();
//		Iterator<TenancyContract> it = tenancyContracts.iterator();
//
//		while(it.hasNext()) {
//			TenancyContract mv = it.next();
//
//			if(mv.getApartment().getManager().getId() == ea.getId())
//				ret.add(mv);
//		}
//
//		return ret;
//	}

	/**
	 * Finds all purchase contracts relating to the houses of a given estate agent
	 * @param  ea The estate agent
	 * @return set of purchase contracts
	 */
//	public Set<PurchaseContract> getPurchaseContractByEstateAgent(EstateAgent ea) {
//		Set<PurchaseContract> ret = new HashSet<PurchaseContract>();
//		Iterator<PurchaseContract> it = purchaseContracts.iterator();
//
//		while(it.hasNext()) {
//			PurchaseContract k = it.next();
//
//			if(k.getHouse().getManager().getId() == ea.getId())
//				ret.add(k);
//		}
//
//		return ret;
//	}


	/**
	 * Deletes a tenancy contract
	 * @param tc the tenancy contract
	 */
//	public void deleteTenancyContract(TenancyContract tc) {
//		apartments.remove(tc);
//	}

	/**
	 * Deletes a purchase contract
//	 * @param tc the purchase contract
	 */
//	public void deletePurchaseContract(PurchaseContract pc) {
//		apartments.remove(pc);
//	}

	/**
	 * Adds some test data
	 */
	public void addTestData() {

		EstateAgent m = new EstateAgent();
		m.setName("Max Mustermann");
		m.setAddress("Am Informatikum 9");
		m.setLogin("max");
		m.setPassword("max");

		this.addEstateAgent(m);

		Person p1 = new Person();
		p1.setAddress("Informatikum");
		p1.setName("Mustermann");
		p1.setFirstname("Erika");


		Person p2 = new Person();
		p2.setAddress("Reeperbahn 9");
		p2.setName("Albers");
		p2.setFirstname("Hans");

		this.savePerson(p1);
		this.savePerson(p2);

		House h = new House();
		h.setCity("Hamburg");
		h.setPostalcode(22527);
		h.setStreet("Vogt-Kölln-Street");
		h.setStreetnumber("2a");
		h.setSquareArea(384);
		h.setFloors(5);
		h.setPrice(10000000);
		h.setGarden(true);
		h.setManager(m);

		this.saveHouse(h);

		Apartment w = new Apartment();
		w.setCity("Hamburg");
		w.setPostalcode(22527);
		w.setStreet("Vogt-Kölln-Street");
		w.setStreetnumber("3");
		w.setSquareArea(120);
		w.setFloor(4);
		w.setRent(790);
		w.setKitchen(true);
		w.setBalcony(false);
		w.setManager(m);
		this.saveApartment(w);

		w = new Apartment();
		w.setCity("Berlin");
		w.setPostalcode(22527);
		w.setStreet("Vogt-Kölln-Street");
		w.setStreetnumber("3");
		w.setSquareArea(120);
		w.setFloor(4);
		w.setRent(790);
		w.setKitchen(true);
		w.setBalcony(false);
		w.setManager(m);
		this.saveApartment(w);

		PurchaseContract pc = new PurchaseContract();
		pc.setHouse(h);
		pc.setContractPartner(p1);
		pc.setContractNo(9234);
		pc.setDate(new Date(System.currentTimeMillis()));
		pc.setPlace("Hamburg");
		pc.setNoOfInstallments(5);
		pc.setIntrestRate(4);
		this.addPurchaseContract(pc);

		TenancyContract tc = new TenancyContract();
		tc.setApartment(w);
		tc.setContractPartner(p2);
		tc.setContractNo(23112);
		tc.setDate(new Date(System.currentTimeMillis()-1000000000));
		tc.setPlace("Berlin");
		tc.setStartDate(new Date(System.currentTimeMillis()));
		tc.setAdditionalCosts(65);
		tc.setDuration(36);
		this.addTenancyContract(tc);

	}
}
