package de.dis2018.core;

import org.hibernate.Session;

public interface SessionExecution {
    void exec(Session session);
}
